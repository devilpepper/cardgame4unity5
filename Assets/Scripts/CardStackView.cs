﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CardStack))]
public class CardStackView : MonoBehaviour {

    CardStack cardStack;
    List<int> fetchedCards;
    int lastCount;

    public Vector3 start;
    public float cardOffset;
    public GameObject cardPrefab;

    void Start()
    {
        fetchedCards = new List<int>();
        cardStack = GetComponent<CardStack>();
        //cardStack.CreateDeck();
        ShowCards();
        lastCount = cardStack.CardCount;
    }

    void Update()
    {
        if(cardStack.CardCount != lastCount)
        {
            lastCount = cardStack.CardCount;
            ShowCards();
        }
    }

    void ShowCards()
    {
        int cardCount = 0;

        float co;
        if (cardStack.HasCards)
        {
            foreach (int i in cardStack.GetCards())
            {
                co = cardOffset * cardCount;

                Vector3 temp = start + new Vector3(co, 0f);

                addCard(temp, i, cardCount);

                cardCount++;
            }
        }
    }

    void addCard(Vector3 position, int cardIndex, int positionalIndex)
    {
        if (fetchedCards.Contains(cardIndex)) return;

        GameObject cardCopy = (GameObject)Instantiate(cardPrefab);

        cardCopy.transform.position = position;

        CardModel cardModel = cardCopy.GetComponent<CardModel>();
        cardModel.cardIndex = cardIndex;
        cardModel.Toggleface(true);

        SpriteRenderer spriteRenderer = cardCopy.GetComponent<SpriteRenderer>();
        spriteRenderer.sortingOrder = positionalIndex;

        fetchedCards.Add(cardIndex);
    }
}
