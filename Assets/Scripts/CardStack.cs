﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardStack : MonoBehaviour {

    // Use this for initialization
    List<int> cards;
    
    public IEnumerable<int> GetCards()
    {
        foreach(int i in cards)
        {
            yield return i;
        }
    }

    public bool isGameDeck;

    public bool HasCards
    {
        get { return cards != null && cards.Count > 0; }
    }

    public int CardCount
    {
        get { return (cards == null ? 0 : cards.Count); }
    }

    public int pop()
    {
        int card = cards[0];
        cards.RemoveAt(0);
        return card;
    }

    public void push(int card)
    {
        cards.Add(card);
    }

    public void CreateDeck()
    {
        cards.Clear();
        for(int i=0; i<52; i++)
        {
            cards.Add(i);
        }
        int n = cards.Count, k, temp;

        while (n-- > 1)
        {
            k = Random.Range(0, n + 1);
            temp = cards[k];
            cards[k] = cards[n];
            cards[n] = temp;
        }
    }

	void Start () {
        cards = new List<int>();
        if(isGameDeck) CreateDeck();
	}
}
