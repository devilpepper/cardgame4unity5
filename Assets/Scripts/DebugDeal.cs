﻿using UnityEngine;
using System.Collections;

public class DebugDeal : MonoBehaviour {

    public CardStack dealer, player;

    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 256, 28), "Hit Me!"))
        {
            player.push(dealer.pop());
        }
    }
}
