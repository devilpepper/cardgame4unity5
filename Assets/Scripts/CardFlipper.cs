﻿using UnityEngine;
using System.Collections;

public class CardFlipper : MonoBehaviour {

    SpriteRenderer spriteRenderer;
    CardModel cardModel;

    public AnimationCurve scaleCurve;
    public float duration = 0.5f;

    void Awake()
    {
        //Just getting components attached to this.
        //Do not use constructors. Use Awake or Start instead
        spriteRenderer = GetComponent<SpriteRenderer>();
        cardModel = GetComponent<CardModel>();
    }

    public void FlipCard(Sprite startImage, Sprite endImage, int cardIndex)
    {
        StopCoroutine(Flip(startImage, endImage, cardIndex));
        StartCoroutine(Flip(startImage, endImage, cardIndex));
    }

    //Co-routine.
    IEnumerator Flip(Sprite startImage, Sprite endImage, int cardIndex)
    {
        spriteRenderer.sprite = startImage;
        float time = 0f;
        float scale;

        bool flipped = false;
        while(time<=1f)
        {
            scale = scaleCurve.Evaluate(time);
            time += Time.deltaTime / duration;

            //scale the object
            Vector3 localScale = transform.localScale;
            localScale.x = scale;
            transform.localScale = localScale;

            if(time >= 0.5f && !flipped)
            {
                spriteRenderer.sprite = endImage;
                flipped = true;
            }
            yield return new WaitForFixedUpdate();
        }

        if(cardIndex == -1)
        {
            cardModel.Toggleface(false);
        }
        else
        {
            cardModel.cardIndex = cardIndex;
            cardModel.Toggleface(true);
        }
    }
}
